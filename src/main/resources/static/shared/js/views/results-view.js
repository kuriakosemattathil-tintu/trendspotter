var app = app || {};

app.ResultsView = Backbone.View.extend({
	events: {
		"change #sort-box select": "sortResults",
		"keyup #search-box": "search",
		"change #search-box": "search",
	},

	filterSortTrends: function() {
        this.tags = this.searchQuery.toLowerCase().replace(/[^a-z\d\s]/g, "").split(" ");

        var tags = this.tags;
        var filterPredicate = function (r) {
            tags: for (var i in tags) {
                var tag = tags[i];
                if (tag === "") {
                    continue;
                }

                if (_containsQuery(r.get('name'), tag)) {
                    continue;
                }

                console.log(r.get('categories'));
                if (_containsCategory(r.get('categories'), tag)) {
                    continue;
                }

                return false;
            }

            return true;
        };
        var filteredList;
        if (this.searchQuery != "") {
            filteredList = this.trends.filter(filterPredicate);
        } else {
            filteredList = this.trends.models;
        }
		var newTrends = new app.Trends(_.first(filteredList, 150));
		return newTrends;
	},

	sortResults: function() {
		this.sortType = $("#sort-box select").val();
		this.renderList();

		if (desktop) {
			this.updateMapBounds();
		}
	},

	search: function() {
		this.searchQuery = $("#search-box").val();
		this.renderList();

		this.renderList();

		if (desktop) {
			this.updateMapBounds();
		}
	},

	selectTrendRoute: function(e) {
		var index = $(e.currentTarget).index();
		app.trendOnRoute = this.sortedTrends[index];
	},

	updateMapBounds: function(e) {
		if (typeof this.sortedTrends !== 'undefined'
				&& this.sortedTrends.length > 0) {
			app.bounds = new google.maps.LatLngBounds();
			this.sortedTrends.forEach(function (r) {
				var latLng = new google.maps.LatLng(r.get('latLng').lat, r.get('latLng').lng);
				app.bounds.extend(latLng);
			});
			app.map.fitBounds(app.bounds);
			var zoom = app.map.getZoom();
			zoom = (zoom > 15) ? 15 : zoom;
			app.map.setZoom(zoom);
		}
	},

	trendHovered: function(index) {
		if (desktop) {
			this.infowindow.close();
			this.infowindow = new google.maps.InfoWindow();
			var r = this.sortedTrends[index];
			this.infowindow.setContent('<b>' + r.get('name') + '</b>');
			this.infowindow.open(app.map, app.markers[index]);
		}
	},

	showPins: function() {
		if (desktop) {
			if (typeof app.markers !== 'undefined') {
				this.drawMarkers(null);
			}
			app.markers = [];
			app.bounds = new google.maps.LatLngBounds();
			this.infowindow = new google.maps.InfoWindow();
			var marker;
			this.sortedTrends.forEach(function (r) {
				var latLng = new google.maps.LatLng(r.get('latLng').lat, r.get('latLng').lng);
				app.bounds.extend(latLng);
				marker = new google.maps.Marker({
					position: latLng,
					map: app.map
				});
				app.markers.push(marker);
				google.maps.event.addListener(marker, 'click', (function(marker, r) {
					return function() {
                        var ll = r.get('latLng');
                        var lat = ll.lat;
                        var lng = ll.lng;
                        var text = "<b>";
                        for (var i = 0; i < this.sortedTrends.length; i++) {
                            if (this.sortedTrends[i].get('latLng').lat == lat && this.sortedTrends[i].get('latLng').lng == lng) {
                                text += this.sortedTrends[i].get('name');
                                var tweet_volume = this.sortedTrends[i].get('tweet_volume');
                                if (tweet_volume != -1) {
                                    text += " (" + this.sortedTrends[i].get('tweet_volume') + " tweets)";
                                }
                                text += "<br/>"
                            };
                        }
                        text += "</b>"
                        this.infowindow.setContent('<b>' + text + '</b>');
						this.infowindow.open(app.map, marker);

						//app.trendOnRoute = r;
						//app.router.navigate("trends/" + r.get('id'), {trigger: true});
					}.bind(this);
				}.bind(this))(marker, r));
			}.bind(this));
			this.drawMarkers(app.map);
		}
	},

	drawMarkers: function(map) {
		for(var i = 0; i < app.markers.length; i++) {
			app.markers[i].setMap(map);
		}
	},

	initialize: function() {
		this.searchQuery = "";
		this.sortType = "special";
	},

	renderList: function() {
		this.sortedTrends = this.filterSortTrends().models;

		this.listView.sortedTrends = this.sortedTrends;
		this.listView.tags = this.tags;

		this.listView.render(function(v) {
			$(this.el).find("ol.trend-list").html(v.el);
		}.bind(this));

		this.showPins();
	},

	render: function(callback) {
		app.getTemplate("pages/results", function(file) {
			var template = _.template(file);

			app.bounds = new google.maps.LatLngBounds();
			this.sortedTrends = this.filterSortTrends().models;

			var html = template({ trends: this.sortedTrends, currentLoc: app.currentLoc });
			$(this.el).html(html);

			callback(this);

			this.listView = new app.ListView();
			this.listView.resultsView = this;
			this.renderList();

			$("#search-box").val(this.searchQuery);

			if (desktop) {
				$('#sort-box').affix({
					offset: {
						top: 0
					},
					target: "#main-wrapper"
				});
			}

			if (desktop) {
				$('#sort-box').on('affix.bs.affix', function() {
					$('ol.trend-list').css('padding-top', $("#sort-box").outerHeight());
				});

				$('#sort-box').on('affix-top.bs.affix', function() {
					$('ol.trend-list').css('padding-top', 0);
				});
			}
		}.bind(this));
	},

	beforeClose: function() {
		if (desktop) {
			this.updateMapBounds();
		}
		
		this.listView.close();
	},
});
