var app = app || {};

app.SearchView = Backbone.View.extend({
	events: {
		"click a#find-curr-location": "getCurrentLoc",
		"click #time-options .btn-row a.btn": "getWithBtnTime",
		"keyup input": "toggleBtnsIfNeeded",
		"change input": "toggleBtnsIfNeeded",
	},

	render: function(callback) {
		app.getTemplate("pages/search", function(file) {
			var template = _.template(file);
			$(this.el).html(template());
			callback(this);

			var start = document.getElementById('curr-location');
			var end = document.getElementById('destination-field');
			var options = { types: ['geocode'] };
			app.autocompleteStart = new google.maps.places.Autocomplete(start, options);
			app.autocompleteEnd = new google.maps.places.Autocomplete(end, options);

			google.maps.event.addListener(app.autocompleteStart, 'place_changed', this.adjustTimeOptions.bind(this));
			google.maps.event.addListener(app.autocompleteEnd, 'place_changed', this.adjustTimeOptions.bind(this));

			if (app.userStart) {
				$("#curr-location").val(app.userStart);
			}

			if (app.userDestination) {
				$("#destination-field").val(app.userDestination);
			}

			this.toggleBtnsIfNeeded();
		}.bind(this));
		if (typeof app.directionsDisplay !== 'undefined') {
			app.directionsDisplay.setMap(null);
		}
		if (typeof app.markers !== 'undefined') {
			for(var i = 0; i < app.markers.length; i++) {
				app.markers[i].setMap(null);
			}
		}
	},

	getCurrentLoc: function(e) {
		e.preventDefault();

		$("#find-curr-location").html('<i class="fa fa-spinner fa-spin"></i>');

		this.refreshLoc();
	},

	refreshLoc: function() {
		navigator.geolocation.getCurrentPosition(function(p) {
			var loc = {
				lat: p.coords.latitude,
				lng: p.coords.longitude
			};
			app.currentLoc = loc;

			$("#find-curr-location").html('<i class="fa fa-location-arrow">');

			var latLng = new google.maps.LatLng(loc.lat, loc.lng);
			app.geocoder.geocode({'latLng': latLng}, function(results, status) {
				var msg;
				if (status == google.maps.GeocoderStatus.OK && results[1]) {
					msg = results[1].formatted_address;
				} else {
					msg = p.coords.latitude + ", " + p.coords.longitude
				}
				$("#curr-location").val(msg);

				this.toggleBtnsIfNeeded();
				this.adjustTimeOptions();
			}.bind(this));
		}.bind(this));
	},

	toggleBtnsIfNeeded: function() {
		var currVal = $("#curr-location").val();
		var destVal = $("#destination-field").val();

		if ((currVal != undefined && currVal.length > 0) &&
			(destVal != undefined && destVal.length > 0)) {
			$("#time-options.disabled").removeClass("disabled");
		} else {
			$("#time-options").addClass("disabled");
		}
	},

	getWithBtnTime: function(e) {
		e.preventDefault();

		var time = parseInt($(e.currentTarget).attr('data-time'));

		this.getResults(time);
	},

	getResults: function(time) {
		find = function() {
			app.foundTrends = new app.Trends();
			app.userStart = $("#curr-location").val();
			app.foundTrends.lat = app.currentLoc.lat;
			app.foundTrends.lng = app.currentLoc.lng;
			app.userDestination = $("#destination-field").val();
			app.foundTrends.destination = app.userDestination;
			app.foundTrends.time = time;
			app.foundTrends.fetch({
				success: function() {
					app.resultsView = undefined;
					app.router.navigate("results", { trigger: true });
				},
				error: function(collection, response, opts) {
					alert(response.responseJSON.error);

					this.render(function() {});

					app.router.navigate("", { trigger: true });
				}.bind(this)
			});

			$(this.el).html('<p class="loading"><i class="fa fa-spinner fa-spin"></i></p>');

			if (desktop) {
				var start = new google.maps.LatLng(app.currentLoc.lat, app.currentLoc.lng);
				var request = {
					origin: start,
					destination: app.foundTrends.destination,
					travelMode: google.maps.TravelMode.DRIVING
				};
				app.directionsService.route(request, function(res, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						app.directionsDisplay.setMap(app.map);
						app.directionsDisplay.setDirections(res);
					}
				});
			}
		}.bind(this)

		if (!app.currentLoc) {
			app.geocoder.geocode({'address': $("#curr-location").val()}, function(result, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					var res = result[0].geometry.location;
					app.currentLoc = {
						lat: res.lat(),
						lng: res.lng()
					}

					find();
				}
			});
		} else {
			find();
		}
	}
});