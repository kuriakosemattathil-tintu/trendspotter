var app = app || {};

app.ListView = Backbone.View.extend({
	events: {
		"mousedown li a": "selectTrendRoute",
		"click li a": "selectTrendRoute",
		"mouseover li": "hoverTrend"
	},

	selectTrendRoute: function(e) {
		e.preventDefault();

		var index = $(e.currentTarget).parent().index();
		app.trendOnRoute = this.sortedTrends[index];

		app.router.navigate("trends/" + app.trendOnRoute.get('id'), { trigger: true });

		return false;
	},

	hoverTrend: function(e) {
		var index = $(e.currentTarget).index();

		this.resultsView.trendHovered(index);
	},

	render: function(callback) {
		app.getTemplate("trends/list", function(file) {
			var template = _.template(file);

			var html = template({ trends: this.sortedTrends, tags: this.tags });

			$(this.el).html(html);
			callback(this);

			//$(".trend-details h3").highlight(this.tags);

			this.delegateEvents();
		}.bind(this));
	}
});
