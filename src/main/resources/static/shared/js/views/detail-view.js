var app = app || {};

app.DetailView = Backbone.View.extend({
	render: function(callback) {
		var draw = function() {
			app.getTemplate("trends/detail", function(file) {
				var template = _.template(file);
				var html = template({ trend: this.trend, ror: this.trendOnRoute, currentLoc: app.currentLoc });

				$(this.el).html(html);
				callback(this);
			}.bind(this));
		}.bind(this);

		if (this.trendOnRoute.get('timeAdded')) {
			draw();
		} else {
			$.get('/time', {lat: app.currentLoc.lat, lng: app.currentLoc.lng, waypoint: this.trend.get('address'), destination: app.userDestination}, function(data) {
				this.trendOnRoute.set('timeAdded', data.time);
				draw();
			}.bind(this));
		}
	}
});