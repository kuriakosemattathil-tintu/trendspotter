var app = app || {};

var Router = Backbone.Router.extend({
	routes: {
		"": "search",
		"results": "list-trends",
		"trends/:id": "show-trend"
	},

	showView: function(selector, view) {
		if (this.currentView) {
			this.currentView.close(view);
		}

		this.currentView = view;
		view.render(function(v) {
			$(selector).html(v.el);
		});
	}
});

app.router = new Router();

app.router.on('route:search', function() {
	$("#back-btn").hide();

	app.router.routesHit = 0;

	document.title = 'TrendSpotter';
	
	app.currentLoc = null;
	app.foundTrends = null;

	var searchView = new app.SearchView();
	searchView.shouldClearMap = true;
	app.router.showView("#main-wrapper", searchView);
});

app.router.on('route:list-trends', function() {
	document.title = "Trends";

	if (app.foundTrends == null) {
		app.router.navigate("", { trigger: true });
		return;
	}

	$("#back-btn").show();
	$("#back-btn").unbind("click");
	$("#back-btn").click(function(e) {
		e.preventDefault();

		app.router.navigate("", {trigger: true});
	});

	var resultsView = new app.ResultsView();

	if (app.resultsView == undefined) {
		resultsView.trends = app.foundTrends;
	} else {
		resultsView.trends = app.resultsView.trends;
		resultsView.searchQuery = app.resultsView.searchQuery;
		resultsView.sortType = app.resultsView.sortType;
	}

	app.resultsView = resultsView;
	app.router.showView("#main-wrapper", resultsView);
});

app.router.on('route:show-trend', function(id) {
	if (app.foundTrends == null) {
		app.router.navigate("", { trigger: true });
	} else {
		$("#back-btn").show();
		$("#back-btn").unbind("click");
		$("#back-btn").click(function(e) {
			e.preventDefault();

			app.router.navigate("results", {trigger: true});
		});

		var trend = new app.Trend({id: id});

		trend.fetch({success: function() {
			var detailView = new app.DetailView();
			detailView.trend = trend;

			if (app.trendOnRoute) {
				detailView.trendOnRoute = app.trendOnRoute;
			}

			document.title = trend.get('name');

			app.router.showView("#main-wrapper", detailView);
		}});
	}
});

app.router.on('route:error', function() {
	alert("Error 404!");
});

Backbone.history.start();
