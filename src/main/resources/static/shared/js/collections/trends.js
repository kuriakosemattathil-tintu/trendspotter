var app = app || {};

app.Trends = Backbone.Collection.extend({
	model: app.Trend,

	url: function() {
		return'/trends?lat=' + this.lat
		+ '&lng=' + this.lng
		+ '&destination=' + encodeURIComponent(this.destination)
	}
});