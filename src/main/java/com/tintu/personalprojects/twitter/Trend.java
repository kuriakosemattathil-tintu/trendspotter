package com.tintu.personalprojects.twitter;

import com.tintu.personalprojects.map.LatLng;
import org.json.simple.JSONObject;

/**
 * Created by prudhvi.chaganti on 11/28/17.
 */
public class Trend implements Comparable<Trend> {
    private String name;
    private String url;
    private String query;
    private String promoted_content;
    private long tweet_volume;
    private LatLng latLng;
    private String bestTweet;
    private String[] categories;

    public Trend(String name, String url, String query, String promoted_content, long tweet_volume, LatLng latLng) {
        this.name = name;
        this.url = url;
        this.query = query;
        this.promoted_content = promoted_content;
        this.tweet_volume = tweet_volume;
        this.latLng = latLng;
    }

    public Trend(JSONObject jsonTrend, LatLng latLng) {
        Object name = jsonTrend.get("name");
        this.name = name != null ? name.toString() : null;
        Object url = jsonTrend.get("url");
        this.url = url != null ? url.toString() : null;
        Object query = jsonTrend.get("query");
        this.query = query != null ? query.toString() : null;
        Object promoted_content = jsonTrend.get("promoted_content");
        this.promoted_content = promoted_content != null ? promoted_content.toString() : null;
        Object tweet_volume = jsonTrend.get("tweet_volume");
        this.tweet_volume = tweet_volume != null ? Long.parseLong(tweet_volume.toString()) : -1;
        this.latLng = latLng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getPromoted_content() {
        return promoted_content;
    }

    public void setPromoted_content(String promoted_content) {
        this.promoted_content = promoted_content;
    }

    public long getTweet_volume() {
        return tweet_volume;
    }

    public void setTweet_volume(long tweet_volume) {
        this.tweet_volume = tweet_volume;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }


    public String getBestTweet() {
        return bestTweet;
    }

    public void setBestTweet(String bestTweet) {
        this.bestTweet = bestTweet;
    }

    public String[] getCategories() {
        return categories == null ? new String[0] : categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public boolean equals(Trend t) {
        return this.name.equals(t.getName());
    }

    @Override
    public int compareTo(Trend t) {
        if (this.tweet_volume == t.getTweet_volume()) {
            return 0;
        } else {
            return this.tweet_volume - t.getTweet_volume() > 0 ? -1 : 1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (obj instanceof Trend) {
            Trend t = (Trend) obj;
            return equals(t);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "Trend [name=" + name + ", url=" + url + ", promoted_content=" + promoted_content + ", query=" + query
                + ", tweet_volume=" + tweet_volume + "]";
    }
}
