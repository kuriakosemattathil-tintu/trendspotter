package com.tintu.personalprojects.twitter;

import com.tintu.personalprojects.apikeys.TwitterAPIKeys;
import com.tintu.personalprojects.cache.Cache;
import com.tintu.personalprojects.cache.CacheFactory;
import com.tintu.personalprojects.map.LatLng;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by prudhvi.chaganti on 11/18/17.
 */
public class TwitterAPI {
    private static final long TIME_THRESHOLD = 15 * 60 * 1000;
    private static final int TWEET_COUNT = 5;
    private static final int TWEET_RADIUS = 100;
    private static final String API_HOST = "api.twitter.com";
    private static final String LOCATIONS_SEARCH_PATH = "/1.1/trends/closest.json";
    private static final String TRENDS_SEARCH_PATH = "/1.1/trends/place.json";
    private static final String TWEETS_SEARCH_PATH = "/1.1/search/tweets.json";

    private volatile static Cache<String, String> latLngToWoeidListMap = CacheFactory.getTimeBasedCache(TIME_THRESHOLD);
    private volatile static Cache<String, String> woeidToTrendsListMap = CacheFactory.getTimeBasedCache(TIME_THRESHOLD);
    private volatile static Cache<String, String> queryToTweetsMap = CacheFactory.getTimeBasedCache(TIME_THRESHOLD);

    OAuthService service;
    Token accessToken;

    public TwitterAPI(TwitterAPIKeys twitterAPIKeys) {
        this.service =
                new ServiceBuilder().provider(TwoStepOAuth.class).apiKey(twitterAPIKeys.getConsumerKey())
                        .apiSecret(twitterAPIKeys.getConsumerSecret()).build();
        this.accessToken = new Token(twitterAPIKeys.getToken(), twitterAPIKeys.getTokenSecret());
    }

    // List of woeid's around the latlong given
    public String getLocations(String lat, String lng) {
        String latLng = lat + lng;
        String woeidList = latLngToWoeidListMap.get(latLng);
        if (woeidList == null) {
            OAuthRequest request = createOAuthRequest(LOCATIONS_SEARCH_PATH);
            request.addQuerystringParameter("lat", lat);
            request.addQuerystringParameter("long", lng);
            woeidList = sendRequestAndGetResponse(request);
            latLngToWoeidListMap.put(latLng, woeidList);
        }
        return woeidList;
    }

    public Set<Trend> getTrends(String woeid, LatLng trendLatLng) {
        String trends = woeidToTrendsListMap.get(woeid);
        if (trends == null) {
            OAuthRequest request = createOAuthRequest(TRENDS_SEARCH_PATH);
            request.addQuerystringParameter("id", woeid);
            trends = sendRequestAndGetResponse(request);
            woeidToTrendsListMap.put(woeid, trends);
        }

        JSONParser parser = new JSONParser();
        JSONArray jsonTrends = null;
        try {
            jsonTrends = (JSONArray) parser.parse(trends);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        jsonTrends = (JSONArray) ((JSONObject) jsonTrends.get(0)).get("trends");
        Set<Trend> trendSet = new TreeSet<>();
        for (int i = 0; i < jsonTrends.size(); i++) {
            trendSet.add(new Trend((JSONObject) jsonTrends.get(i), trendLatLng));
        }
        return trendSet;
    }

    public String getTweets(String query, String lat, String lng) {
        String tweetList = queryToTweetsMap.get(query);
        if (tweetList == null) {
            OAuthRequest request = createOAuthRequest(TWEETS_SEARCH_PATH);
            request.addQuerystringParameter("q", query);
            request.addQuerystringParameter("lang", "en");
            request.addQuerystringParameter("tweet_mode", "extended");
            request.addQuerystringParameter("geocode", lat + "," + lng + "," + TWEET_RADIUS + "mi");
            request.addQuerystringParameter("count", Integer.toString(TWEET_COUNT));
            tweetList = sendRequestAndGetResponse(request);
            queryToTweetsMap.put(query, tweetList);
        }
        return tweetList;
    }

    public String filterTweets(String tweetList) {
        JSONParser parser = new JSONParser();
        JSONArray jsonTweets = null;
        try {
            jsonTweets = (JSONArray) ((JSONObject) parser.parse(tweetList)).get("statuses");
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        String bestTweet = null;
        int maxLength = 0;
        for (int i = 0; i < jsonTweets.size(); i++) {
            String fullText = (String) ((JSONObject) jsonTweets.get(i)).get("full_text");
            if (maxLength < fullText.length()) {
                bestTweet = fullText;
                maxLength = fullText.length();
            }
            // TODO Use retweetCount while determining best tweet
            //int retweetCount = (Integer) ((JSONObject) jsonTweets.get(i)).get("retweet_count");
        }
        return bestTweet;
    }

    private OAuthRequest createOAuthRequest(String path) {
        OAuthRequest request = new OAuthRequest(Verb.GET, "https://" + API_HOST + path);
        return request;
    }

    private String sendRequestAndGetResponse(OAuthRequest request) {
        this.service.signRequest(this.accessToken, request);
        Response response = request.send();
        return response.getBody();
    }

    public static void main(String args[]) {
        TwitterAPI twitterAPI = new TwitterAPI(new TwitterAPIKeys(args[0], args[1], args[2], args[3]));
        System.out.println(twitterAPI.getLocations("33.962880000000006", "-117.02769"));
    }
}