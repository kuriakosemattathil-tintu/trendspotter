package com.tintu.personalprojects.cache;

public class CacheFactory {
    public static <K, V> Cache getTimeBasedCache(long thresholdInMillis) {
        return new TimeBasedCache(thresholdInMillis);
    }
}
