package com.tintu.personalprojects.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TimeBasedCache<K, V> implements Cache<K, V> {
    private Map<K, TimeBasedCacheValue<V>> map = new ConcurrentHashMap<>();

    private long threshold;

    // TODO Cap cache based on number of characters stored and evict based on time in cache
    public TimeBasedCache(long threshold) {
        this.threshold = threshold;
    }

    @Override
    public void put(K key, V value) {
        map.put(key, new TimeBasedCacheValue(value, System.currentTimeMillis()));
    }

    @Override
    public V get(K key) {
        if (map.containsKey(key)) {
            if (System.currentTimeMillis() - map.get(key).getTime() > threshold) {
                System.out.println("Cache miss (expired) for " + key);
                map.remove(key);
                return null;
            }
            System.out.println("Cache hit for key " + key);
            return map.get(key).getValue();
        } else {
            System.out.println("Cache miss for " + key);
            return null;
        }
    }

    private class TimeBasedCacheValue<V> {
        private V value;
        private long time;

        public TimeBasedCacheValue(V value, long time) {
            this.value = value;
            this.time = time;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }
    }
}
