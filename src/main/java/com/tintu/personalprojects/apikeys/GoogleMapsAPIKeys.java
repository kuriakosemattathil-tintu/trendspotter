package com.tintu.personalprojects.apikeys;

public class GoogleMapsAPIKeys {
    private String apiKey;

    public GoogleMapsAPIKeys(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }
}
