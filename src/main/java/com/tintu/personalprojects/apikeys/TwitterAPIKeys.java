package com.tintu.personalprojects.apikeys;

public class TwitterAPIKeys {
    private String consumerKey;
    private String consumerSecret;
    private String token;
    private String tokenSecret;

    public TwitterAPIKeys(String consumerKey, String consumerSecret, String token, String tokenSecret) {
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.token = token;
        this.tokenSecret = tokenSecret;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public String getToken() {
        return token;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }
}
