package com.tintu.personalprojects.map;

public interface RouteFinder {
  /**
   * Gets a route between a start location and an end andress
   *
   * @param start
   *          The start location of the route.
   * @param address
   *          The human-readable address of the route.
   * @return A Route object giving the route between start and address.
   * Returns null if no route can be found.
   */
  GoogleRoute getRoute(LatLng start, String address);

  /** Gets a route between a start address and an end address.
   * @param startAddress The start address of the route.
   * @param destinationAddress The end address of the route.
   * @return A Route object giving the route between startAddress
   * and endAddress. Returns null if no route can be found.
   */
  GoogleRoute getRoute(String startAddress, String destinationAddress);
}
