package com.tintu.personalprojects.map;

import com.google.maps.model.DirectionsRoute;

import java.util.List;
import java.util.stream.Collectors;

public class GoogleRoute {

  private List<LatLng> polylinePoints;

  public GoogleRoute(DirectionsRoute[] routes) {
    assert routes.length >= 1;

    DirectionsRoute gRoute = routes[0];
    assert gRoute.legs.length == 1;

    polylinePoints = gRoute.overviewPolyline.decodePath().stream()
            .map(p -> new LatLng(p))
            .collect(Collectors.toList());
  }

  public List<LatLng> getPolylinePoints() {
    return polylinePoints;
  }

  public void setPolylinePoints(List<LatLng> polylinePoints) {
    this.polylinePoints = polylinePoints;
  }
}