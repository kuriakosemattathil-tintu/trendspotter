package com.tintu.personalprojects.map;

public class LatLng {
  private final Double lat;
  private final Double lng;

  /**
   * Constructs a latitude and longitude object.
   * @param lat the latitude.
   * @param lng the longitude.
   */
  public LatLng(Double lat, Double lng) {
    this.lat = lat;
    this.lng = lng;
  }

  // Convert from Google LatLng
  protected LatLng(com.google.maps.model.LatLng gLoc) {
    this.lat = gLoc.lat;
    this.lng = gLoc.lng;
  }

  /**
   * Returns the latitude.
   * @return returns the double representing the latitude.
   */
  public Double getLat() {
    return lat;
  }

  /**
   * Returns the longitude.
   * @return returns the double representing the longitude.
   */
  public Double getLng() {
    return lng;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    } else if (obj instanceof LatLng) {
      LatLng latLng = (LatLng) obj;
      return this.lat.equals(latLng.lat)
          && this.lng.equals(latLng.lng);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "LatLng [lat=" + lat + ", lng=" + lng + "]";
  }
}
