package com.tintu.personalprojects.map;

import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsRoute;
import com.tintu.personalprojects.apikeys.GoogleMapsAPIKeys;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GoogleRouteFinder implements RouteFinder {
  private final GeoApiContext context;
  private final String apiKey;

  public GoogleRouteFinder(GoogleMapsAPIKeys googleMapsAPIKeys) {
    apiKey = googleMapsAPIKeys.getApiKey();
    this.context = new GeoApiContext().setApiKey(apiKey);
  }

  @Override
  public GoogleRoute getRoute(String startAddress, String destinationAddress) {
    DirectionsRoute[] routes;
    try {
      routes = DirectionsApi.newRequest(context)
              .origin(startAddress)
              .destination(destinationAddress)
              .await();
    } catch (Exception e) {
      return null;
    }

    if (routes.length == 0) {
      return null;
    }
    return new GoogleRoute(routes);
  }

  @Override
  public GoogleRoute getRoute(LatLng start, String destinationAddress) {
    com.google.maps.model.LatLng gLoc = new com.google.maps.model.LatLng(
            start.getLat(), start.getLng());
    DirectionsRoute[] routes;
    try {
      routes = DirectionsApi.newRequest(context)
              .origin(gLoc)
              .destination(destinationAddress)
              .await();
    } catch (Exception e) {
      return null;
    }

    if (routes.length == 0) {
      return null;
    }

    return new GoogleRoute(routes);
  }

  // TODO Cleanup this code. Not caching at this point
  public List<String> getCategories(String text) {
    try {
      HttpClient httpclient = HttpClients.createDefault();
      HttpPost httppost = new HttpPost("https://language.googleapis.com/v1/documents:classifyText?fields=categories&key=" + apiKey);

      JSONObject jsonObject = new JSONObject();
      JSONObject docObject = new JSONObject();
      docObject.put("content", text);
      docObject.put("language", "en");
      docObject.put("type", "PLAIN_TEXT");
      jsonObject.put("document", docObject);
      httppost.setEntity(new StringEntity(jsonObject.toString()));

      //Execute and get the response.
      HttpResponse response = null;
      response = httpclient.execute(httppost);
      //System.out.println(response.getStatusLine());
      HttpEntity entity = response.getEntity();

      if (entity != null) {
        InputStream instream = entity.getContent();
        try {
          final int bufferSize = 1024;
          final char[] buffer = new char[bufferSize];
          final StringBuilder out = new StringBuilder();
          Reader in = new InputStreamReader(instream, "UTF-8");
          for (; ; ) {
            int rsz = in.read(buffer, 0, buffer.length);
            if (rsz < 0)
              break;
            out.append(buffer, 0, rsz);
          }
          JSONParser parser = new JSONParser();
          JSONArray categoriesJson = null;
          try {
            categoriesJson = (JSONArray) ((JSONObject) parser.parse(out.toString())).get("categories");
          } catch (ParseException pe) {
            pe.printStackTrace();
          }
          List<String> categories = new ArrayList<String>();
          for (int i = 0; i < categoriesJson.size(); i++) {
            String categoryToken = (String) ((JSONObject) categoriesJson.get(i)).get("name");
            categories.addAll(Arrays.asList(categoryToken.split("/")));
          }
          //System.out.println(categories);
          return categories;
        } finally {
          instream.close();
        }
      }
    } catch (Exception e) {

    }
    return null;
  }

  public static void main(String args[]) {
    String googleMapsApiKey = args[0];
    GoogleRouteFinder g = new GoogleRouteFinder(new GoogleMapsAPIKeys(googleMapsApiKey));
    g.getCategories("I will see Beyonce anywhere other than Coachella.. the heat.. the grass.. the outside elements.. \\nI can’t.. inside venues only");
    System.exit(0);
    GoogleRoute gr = g.getRoute("Sunnyvale","Arizona");
    System.out.println(gr.getPolylinePoints());
    System.out.println(gr.getPolylinePoints().size());
  }

}