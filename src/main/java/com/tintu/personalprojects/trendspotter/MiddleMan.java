package com.tintu.personalprojects.trendspotter;

import com.tintu.personalprojects.map.GoogleRoute;
import com.tintu.personalprojects.map.GoogleRouteFinder;
import com.tintu.personalprojects.map.LatLng;
import com.tintu.personalprojects.map.RouteFinder;
import com.tintu.personalprojects.twitter.Trend;
import com.tintu.personalprojects.twitter.TwitterAPI;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.*;

public class MiddleMan {
    private final GoogleRouteFinder map;
    private final TwitterAPI twitterAPI;

    public MiddleMan(GoogleRouteFinder map, TwitterAPI twitterAPI) {
        this.map = map;
        this.twitterAPI = twitterAPI;
    }

    protected Set<Trend> findTrends(LatLng start, String destination) {
        Set<Trend> result = new HashSet<>();
        String woeid = "1";
        LatLng trendLatLng = null;
        GoogleRoute gr = map.getRoute(start, destination);
        System.out.println(gr.getPolylinePoints().size());
        int[] pointIndices = getPointIndices(gr.getPolylinePoints().size(), 3);
        for (int i = 0; i < pointIndices.length; i++) {
            try {
                String locationsHavingTrends = twitterAPI.getLocations(
                        gr.getPolylinePoints().get(pointIndices[i]).getLat().toString(),
                        gr.getPolylinePoints().get(pointIndices[i]).getLng().toString());
                System.out.println("Location having trends = " + locationsHavingTrends);
                Object jsonObject = new JSONParser().parse(locationsHavingTrends);
                if (jsonObject instanceof JSONArray) {
                    JSONArray jsonArray = (JSONArray) jsonObject;
                    System.out.println(jsonArray.get(0));
                    woeid = ((JSONObject) jsonArray.get(0)).get("woeid").toString();
                } else {
                    if (jsonObject instanceof JSONObject) {
                        System.out.println(jsonObject);
                    }
                    System.out.println("Likely rate limit error");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (!woeid.equals(1)) {
                System.out.println("woeid: " + woeid);
                trendLatLng = gr.getPolylinePoints().get(pointIndices[i]);
                result.addAll(twitterAPI.getTrends(woeid, trendLatLng));
            }
        }
        populateCategories(result);
        return result;
    }

    public void populateCategories(Set<Trend> trendSet) {
        long startTime = System.currentTimeMillis();
        List<Thread> threads = new ArrayList<>();
        for (Trend trend : trendSet) {
            if (trend.getTweet_volume() != -1) {
                Thread t = new Thread(new TweetFetchWorker(trend, trend.getLatLng()));
                t.start();
                threads.add(t);
            }
        }
        for (Thread t : threads) {
            try {
                t.join(3000);
                // If populate categories is taking more than 10 seconds, return
                if (System.currentTimeMillis() - startTime > 10000) {
                    System.out.println("Killing populateCategories");
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Time taken for populateCategories = " + (System.currentTimeMillis() - startTime) + " ms");
    }

    public int[] getPointIndices(int size, int numPoints) {
        int[] pointIndices = new int[Math.min(size, numPoints)];
        if (size < numPoints) {
            for (int i = 0; i < pointIndices.length; i++) {
                pointIndices[i]++;
            }
        } else {
            //TODO make this random
            for (int i = 1; i <= pointIndices.length; i++) {
                pointIndices[i - 1] = (i * size / numPoints) - 1;
                System.out.println(pointIndices[i - 1]);
            }
        }
        return pointIndices;
    }

    private class TweetFetchWorker implements Runnable {
        Trend trend;
        LatLng latLng;

        public TweetFetchWorker(Trend trend, LatLng latLng) {
            this.trend = trend;
            this.latLng = latLng;
        }

        @Override
        public void run() {
            // Call search API with trend name
            String tweetList = twitterAPI.getTweets(trend.getName(),
                    Double.toString(trend.getLatLng().getLat()), Double.toString(trend.getLatLng().getLng()));
            // Filter for the best tweet
            String bestTweet = twitterAPI.filterTweets(tweetList);
            // pass best tweet to google knowledge graph API and fetch categories
            List<String> categories = map.getCategories(bestTweet);
            if (categories != null) {
                trend.setCategories(categories.toArray(new String[categories.size()]));
            }
        }
    }
}
