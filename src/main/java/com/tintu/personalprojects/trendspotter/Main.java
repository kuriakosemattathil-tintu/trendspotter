package com.tintu.personalprojects.trendspotter;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tintu.personalprojects.apikeys.GoogleMapsAPIKeys;
import com.tintu.personalprojects.apikeys.TwitterAPIKeys;
import com.tintu.personalprojects.map.GoogleRouteFinder;
import com.tintu.personalprojects.map.LatLng;
import com.tintu.personalprojects.map.RouteFinder;
import com.tintu.personalprojects.twitter.Trend;
import com.tintu.personalprojects.twitter.TwitterAPI;
import spark.ExceptionHandler;
import spark.ModelAndView;
import spark.QueryParamsMap;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;
import spark.TemplateViewRoute;
import spark.template.freemarker.FreeMarkerEngine;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import freemarker.template.Configuration;

public final class Main {

    private static final String[] MOBILE_USER_AGENTS = {"Android", "Blackberry",
            "iPhone", "Blazer 3.0", "Opera Mini"};

    public static void main(String[] args) {
        GsonBuilder builder = new GsonBuilder();
        GSON = builder.create();
        new Main(args).run();
    }

    private String[] args;
    private static Gson GSON;
    private MiddleMan middleman;
    private TwitterAPIKeys twitterAPIKeys;
    private GoogleMapsAPIKeys googleMapsAPIKeys;

    private Main(String[] args) {
        // TODO Read API keys from a config and/or environment variables
        this.args = args;
        this.googleMapsAPIKeys = new GoogleMapsAPIKeys(args[0]);
        this.twitterAPIKeys = new TwitterAPIKeys(args[1], args[2], args[3], args[4]);
    }

    private void run() {
        GoogleRouteFinder map = new GoogleRouteFinder(googleMapsAPIKeys);
        TwitterAPI twitterAPI = new TwitterAPI(twitterAPIKeys);
        middleman = new MiddleMan(map, twitterAPI);

        runSparkServer();
    }

    private FreeMarkerEngine createEngine() {
        Configuration config = new Configuration();
        config.setClassForTemplateLoading(this.getClass(),
                "/spark/template/freemarker");

        return new FreeMarkerEngine(config);
    }

    private void runSparkServer() {
        Spark.staticFileLocation("/static");
        Spark.exception(Exception.class, new ExceptionPrinter());

        FreeMarkerEngine freeMarker = createEngine();

        String p = new ProcessBuilder().environment().get("PORT");
        if (p != null) {
            Spark.setPort(Integer.parseInt(p));
        }

        // Setup Spark Routes
        Spark.get("/", new MainHandler(), freeMarker);
        Spark.get("/desktop", new DesktopHandler(), freeMarker);
        Spark.get("/mobile", new MobileHandler(), freeMarker);
        Spark.get("/trends", new TrendHandler());
    }

    private Object errorJSON(String message) {
        Map<String, Object> variables = new ImmutableMap.Builder().put("error",
                message).build();
        return GSON.toJson(variables);
    }


    private class MainHandler implements TemplateViewRoute {
        @Override
        public ModelAndView handle(Request req, Response res) {
            String ua = req.userAgent();

            if (userAgentIsMobile(ua)) {
                return new ModelAndView(null, "mobile.ftl");
            } else {
                return new ModelAndView(null, "desktop.ftl");
            }
        }
    }

    private class MobileHandler implements TemplateViewRoute {
        @Override
        public ModelAndView handle(Request req, Response res) {
            return new ModelAndView(null, "mobile.ftl");
        }
    }

    private class DesktopHandler implements TemplateViewRoute {
        @Override
        public ModelAndView handle(Request req, Response res) {
            return new ModelAndView(null, "desktop.ftl");
        }
    }

    private static boolean userAgentIsMobile(String ua) {
        for (int i = 0; i < MOBILE_USER_AGENTS.length; i++) {
            if (ua.indexOf(MOBILE_USER_AGENTS[i]) >= 0) {
                return true;
            }
        }

        return false;
    }

    private class TrendHandler implements Route {
        @Override
        public Object handle(Request req, Response res) {
            QueryParamsMap qm = req.queryMap();
            Double lat = Double.parseDouble(qm.value("lat"));
            Double lng = Double.parseDouble(qm.value("lng"));
            LatLng loc = new LatLng(lat, lng);

            String destination = qm.value("destination");

            Set<Trend> trends = middleman.findTrends(loc, destination);

            if (trends == null) {
                res.status(400);
                return errorJSON("Could not find trends till " + destination);
            } else {
                res.type("text/json");
                String json = GSON.toJson(trends);
                //System.out.println(json);
                return json;
            }
        }
    }

    private static class ExceptionPrinter implements ExceptionHandler {
        private static final int ERROR_CODE = 500;

        @Override
        public void handle(Exception e, Request req, Response res) {
            res.status(ERROR_CODE);
            StringWriter stacktrace = new StringWriter();
            try (PrintWriter pw = new PrintWriter(stacktrace)) {
                pw.println("<pre>");
                e.printStackTrace(pw);
                pw.println("</pre>");
            }
            res.body(stacktrace.toString());
        }
    }
}
